# README #

### **Aplikacja do zarządzania koszykiem**###

## Opis aplikacji ##

Zadaniem aplikacji jest dodawanie, usuwanie i przechowywanie produktów w koszyku wraz z ich ilością oraz wyliczanie wartości koszyka.

Założeniami wstępnymi projektu są:

1.  Produkt składa się z pól: nazwa, opis, cena.

2.  Koszyk przechowuje listę produktów wraz z ilością.

3.  Ten sam produkt może zostać dodany do koszyka tylko jeden raz (ponowne dodanie tego
samego produktu powinno zwiększyć ilość tego produktu w koszyku).

4.  Koszyk potrafi wyliczyć sumę (kwota oraz ilość) wszystkich dodanych produktów.

Ponadto z koszyka można usunąć wybraną rzecz co jest jednoznaczne ze zmianą wartości koszyka.

## Przyjęte założenia ##

1.  Pola nazwa, opis i cena produktu zawierają się w klasie abstrakcyjnej AbstractProduct, która implementuje interfejs zawierający sygnatury metod.
2. Założyłam, iż poszczególne produkty będę tworzyć za pomocą fabryki. Są one tworzone w klasie main. W klasie main są również wywoływane metody do dodawania i odejmowania produktów oraz tworzona jest instancja samego koszyka.
3. Wykonałam też testy do klas: Basket, ProductFactory oraz ProductImpl. Pokrycie tych klas testami jest 100-procentowe.