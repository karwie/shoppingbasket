package test.java.com.product;

import main.java.com.product.Product;
import main.java.com.product.ProductFactory;
import main.java.com.product.ProductImpl;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

/**
 * Created by Karolina on 2015-11-03.
 */
public class ProductImplTest {
    private Product chilliChips = new ProductImpl("Chilli Chips", "Melting cheese inside!", BigDecimal.valueOf(2.4));
    private Product darkChocolate = new ProductImpl("Dark Chocolate", "60% Cocoa", BigDecimal.valueOf(1.5));

    @Test
    public void productImplCompareToChipsNameTest() {
        final Product expectedValue = chilliChips;
        final Product actualValue = ProductFactory.createOnionChips();
        final int result = expectedValue.compareTo(actualValue);

        assertTrue("productImplCompareToChipsNameTest - the chips names are different", result != 0);
    }

    @Test
    public void productImplCompareToChocolateDescriptionPriceTest() {
        final Product expectedValue = darkChocolate;
        final Product actualValue = ProductFactory.createDarkChocolate();
        final int result = expectedValue.compareTo(actualValue);

        assertTrue("productImplCompareToChocolateDescriptionPriceTest - the chocolate description and prices are different", result != 0);
    }
}