package test.java.com.product;

import main.java.com.product.Product;
import main.java.com.product.ProductFactory;
import main.java.com.product.ProductImpl;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Karolina on 2015-11-03.
 */
public class ProductFactoryTest extends ProductFactory {

    private Product onionChips = new ProductImpl("Onion Chips", "Melting cheese inside!", BigDecimal.valueOf(2.4));
    private Product darkChocolate = new ProductImpl("Dark Chocolate", "90% Cocoa", BigDecimal.valueOf(3.5));
    private Product canOfPepsi = new ProductImpl("Pepsi-Cola", "A can of pepsi cola", BigDecimal.valueOf(1.0));
    private Product darkChocolateOtherName = new ProductImpl("Dark Chocolate with nuts", "90% Cocoa", BigDecimal.valueOf(3.5));
    private Product darkChocolateOtherPrice = new ProductImpl("Dark Chocolate", "90% Cocoa", BigDecimal.valueOf(4.5));
    private Product darkChocolateOtherDescription = new ProductImpl("Dark Chocolate", "50% Cocoa", BigDecimal.valueOf(3.5));

    @Test
    public void productFactoryCreateOnionChipsTest() {
        final Product expectedValue = onionChips;
        final Product actualValue = ProductFactory.createOnionChips();

        final int result = expectedValue.compareTo(actualValue);
        assertTrue("productFactoryCreateOnionChipsTest - expected to be equal", result == 0);
    }

    @Test
    public void productFactoryCreateDarkChocolateTest() {
        final Product expectedValue = darkChocolate;
        final Product actualValue = ProductFactory.createDarkChocolate();

        assertEquals("productFactoryCreateDarkChocolateTest - expected to be equal ", 0, expectedValue.compareTo(actualValue));
    }

    @Test
    public void productFactoryCreateCanOfPepsiTest() {
        final Product expectedValue = canOfPepsi;
        final Product actualValue = ProductFactory.createCanOfPepsi();

        assertEquals("productFactoryCreateCanOfPepsiTest - expected to be equal ", 0, expectedValue.compareTo(actualValue));
    }

    @Test
    public void productFactoryCreateOnionChipsAndDarkChocolateTest() {
        final Product chips = ProductFactory.createOnionChips();
        final Product chocolate = ProductFactory.createDarkChocolate();
        final int result = chips.compareTo(chocolate);

        assertTrue("productFactoryCreateOnionChipsAndDarkChocolateTest - expected to be not equal", result != 0);
    }

    @Test
    public void productFactoryCreateOnionChipsAndCanOfPepsiTest() {
        final Product chips = ProductFactory.createOnionChips();
        final Product pepsi = ProductFactory.createCanOfPepsi();
        final int result = chips.compareTo(pepsi);

        assertTrue("productFactoryCreateOnionChipsAndCanOfPepsiTest - expected to be not equal", result != 0);
    }

    @Test
    public void productFactoryCreateDarkChocolateAndCanOfPepsiTest() {
        final Product chocolate = ProductFactory.createDarkChocolate();
        final Product pepsi = ProductFactory.createCanOfPepsi();
        final int result = chocolate.compareTo(pepsi);

        assertTrue("productFactoryCreateOnionChipsAndCanOfPepsiTest - expected to be not equal", result != 0);
    }

    @Test
    public void productFactoryCreateDarkChocolateOtherNameTest() {
        final Product expectedValue = darkChocolateOtherName;
        final Product actualValue = ProductFactory.createDarkChocolate();
        final int result = expectedValue.compareTo(actualValue);

        assertTrue("productFactoryCreateDarkChocolateOtherNameTest - expected to be not equal", result != 0);
    }

    @Test
    public void productFactoryCreateDarkChocolateOtherDescriptionTest() {
        final Product expectedValue = darkChocolateOtherDescription;
        final Product actualValue = ProductFactory.createDarkChocolate();
        final int result = expectedValue.compareTo(actualValue);

        assertTrue("productFactoryCreateDarkChocolateOtherDescriptionTest - expected to be not equal", result != 0);
    }

    @Test
    public void productFactoryCreateDarkChocolateOtherPriceTest() {
        final Product expectedValue = darkChocolateOtherPrice;
        final Product actualValue = ProductFactory.createDarkChocolate();
        final int result = expectedValue.compareTo(actualValue);

        assertTrue("productFactoryCreateDarkChocolateOtherPriceTest - expected to be not equal", result != 0);
    }
}
