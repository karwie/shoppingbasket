package test.java.com.basket;

import main.java.com.basket.Basket;
import main.java.com.product.Product;
import main.java.com.product.ProductImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Karolina on 2015-11-03.
 */
public class BasketTest {

    private Product onionChips = new ProductImpl("Onion Chips", "Melting cheese inside!", BigDecimal.valueOf(2.4));
    private Product darkChocolate = new ProductImpl("Dark Chocolate", "90% Cocoa", BigDecimal.valueOf(3.5));
    private Product canOfPepsi = new ProductImpl("Pepsi-Cola", "A can of pepsi cola", BigDecimal.valueOf(1.0));

    private Basket actualBasket = new Basket();
    private Map<Product, Integer> expectedBasket = new HashMap<Product, Integer>();
    private BigDecimal expectedBasketValue = BigDecimal.ZERO;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        expectedBasket.clear();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void addProductTest() {
        expectedBasket.put(onionChips, 1);
        final Map expectedValue = expectedBasket;
        actualBasket.addProduct(onionChips);
        final Map actualValue = actualBasket.basketStorage;

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void checkQuantityOfBasketTest() {
        expectedBasket.put(onionChips, 1);
        expectedBasket.put(darkChocolate, 2);
        expectedBasket.put(canOfPepsi, 1);
        final Integer expectedValue = expectedBasket.size();

        actualBasket.addProduct(onionChips);
        actualBasket.addProduct(darkChocolate);
        actualBasket.addProduct(darkChocolate);
        actualBasket.addProduct(canOfPepsi);
        final Integer actualValue = actualBasket.basketStorage.size();

        assertEquals("checkQuantityOfBasketTest - expected to be equal", expectedValue, actualValue);
    }

    @Test
    public void deleteProductTest() {
        expectedBasket.put(onionChips, 2);
        expectedBasket.put(onionChips, expectedBasket.get(onionChips)-1);
        final Map expectedValue = expectedBasket;

        actualBasket.addProduct(onionChips);
        actualBasket.addProduct(onionChips);
        actualBasket.deleteProduct(onionChips);
        final Map actualValue = actualBasket.basketStorage;

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void deleteProductNotInBasketTest() {
        expectedBasket.remove(onionChips);
        final Map expectedValue = expectedBasket;
        actualBasket.deleteProduct(onionChips);
        final Map actualValue = actualBasket.basketStorage;

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void getPriceTest() {
        expectedBasket.put(onionChips, 1);
        expectedBasketValue = expectedBasketValue.add(onionChips.getPrice());
        final BigDecimal expectedValue = expectedBasketValue;

        actualBasket.addProduct(onionChips);
        final BigDecimal actualValue = actualBasket.basketValue;

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void getPriceWithSubtractTest() {
        actualBasket.addProduct(canOfPepsi);
        actualBasket.deleteProduct(canOfPepsi);
        final BigDecimal expectedValue = BigDecimal.valueOf(0.0);
        final BigDecimal actualValue = actualBasket.basketValue;

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void printBasketContentsTest() {
        actualBasket.addProduct(onionChips);
        actualBasket.printBasketContent();

        assertTrue("The output String does not contain 'Onion chips'", (outContent.toString().contains("Onion Chips")));
    }

    @Test
    public void printEmptyBasketTest() {
        actualBasket.printBasketContent();

        assertTrue("Basket value is not 0 PLN", (outContent.toString().contains("0 PLN")));
    }
}
