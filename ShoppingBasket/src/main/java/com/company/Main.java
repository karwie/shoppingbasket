package main.java.com.company;

import main.java.com.basket.Basket;
import main.java.com.product.Product;
import main.java.com.product.ProductFactory;

public class Main {

    public static void main(String[] args) {
        Product darkChocolate = ProductFactory.createDarkChocolate();
        Product onionChips = ProductFactory.createOnionChips();
        Product canOfPepsi = ProductFactory.createCanOfPepsi();
        Basket basket = new Basket();

        basket.addProduct(darkChocolate);
        basket.addProduct(onionChips);
        basket.addProduct(canOfPepsi);
        basket.addProduct(darkChocolate);

        basket.printBasketContent();
        basket.deleteProduct(onionChips);
        basket.printBasketContent();
    }
}
