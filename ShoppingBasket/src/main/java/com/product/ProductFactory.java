package main.java.com.product;

import java.math.BigDecimal;

/**
 * Created by Karolina on 2015-11-03.
 */
public class ProductFactory {

    public static Product createDarkChocolate(){
        return new ProductImpl("Dark Chocolate", "90% Cocoa", BigDecimal.valueOf(3.5));
    }

    public static Product createOnionChips(){
        return new ProductImpl("Onion Chips", "Melting cheese inside!", BigDecimal.valueOf(2.4));
    }

    public static Product createCanOfPepsi(){
        return new ProductImpl("Pepsi-Cola", "A can of pepsi cola", BigDecimal.valueOf(1.0));
    }
}
