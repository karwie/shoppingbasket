package main.java.com.product;

import java.math.BigDecimal;

/**
 * Created by Karolina on 2015-11-03.
 */
public abstract class AbstractProduct implements Product {

    String name;
    String description;
    BigDecimal price;

    public AbstractProduct(String name, String description, BigDecimal price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public final String getName(){
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }
}
