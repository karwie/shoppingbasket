package main.java.com.product;

import java.math.BigDecimal;

/**
 * Created by Karolina on 2015-11-03.
 */
public class ProductImpl extends AbstractProduct {
    public ProductImpl(String name, String description, BigDecimal price) {
        super(name, description, price);
    }

    @Override
    public int compareTo(Product otherProduct) {
        return (this.getName().equals(otherProduct.getName()) &&
                this.getDescription().equals(otherProduct.getDescription()) &&
                this.getPrice().equals(otherProduct.getPrice()) ? 0 : -1);
    }
}
