package main.java.com.product;

import java.math.BigDecimal;

/**
 * Created by Karolina on 2015-11-03.
 */
public interface Product extends Comparable<Product> {

    public String getName();
    public String getDescription();
    public BigDecimal getPrice();
}
