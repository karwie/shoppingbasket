package main.java.com.basket;

import main.java.com.product.Product;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Karolina on 2015-11-03.
 */
public class Basket {

    public Map<Product, Integer> basketStorage;
    public BigDecimal basketValue;

    public Basket(){
        basketStorage = new HashMap<Product, Integer>();
        basketValue = BigDecimal.ZERO;
    }

    public void addProduct(Product product){
        if(basketStorage.containsKey(product)){
            basketStorage.put(product, basketStorage.get(product)+1);
            basketValue = basketValue.add(product.getPrice());
        }
        else{
            basketStorage.put(product, 1);
            basketValue = basketValue.add(product.getPrice());
        }
        System.out.println(product.getName() + " was added. Product description: " + product.getDescription() + ". Total Price: " + basketValue + " PLN");
    }

    public void deleteProduct(Product product) {
        if(basketStorage.containsKey(product)){
            if(basketStorage.get(product).equals(1)){
                basketStorage.remove(product);
                basketValue = basketValue.subtract(product.getPrice());
            }
            else if (basketStorage.containsKey(product)) {
                basketStorage.put(product, basketStorage.get(product)-1);
                basketValue = basketValue.subtract(product.getPrice());
            }
        }
        else{
            System.out.println("The basket doesn't contain this product.");
        }
    }

    public void printBasketContent(){
        System.out.println("\nBasket contains: ");
        for( Map.Entry<Product, Integer> entry : basketStorage.entrySet()){
            System.out.println(entry.getKey().getName()+" : "+ entry.getValue() + " pcs.");
        }
        System.out.println("\nTotal Price: " + basketValue + " PLN");
        System.out.println("------------------------");
    }
}
